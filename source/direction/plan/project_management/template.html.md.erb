---
layout: markdown_page
title: "Product Strategy - Project Management"
---

- TOC
{:toc}

## Overview

This page contains the product strategy for the Project Management group. The Project Management group enables teams to collaborate together on plans and requirements through issues and issue boards. The Project Management group is responsible for two distinct product categories:

| Category | Description |
| ------ |  ------ |
| [Issue Tracking](/direction/plan/issue_tracking) | Discuss implementations with your team within issues  |
| [Issue Boards](/direction/plan/kanban_boards) | Organize and prioritize your workflow |
| [Time Tracking](/direction/plan/time_tracking) | Track estimate time and time spent on the conclusion of an issue or merge request |

If you'd like to discuss this strategy directly with the product manager for [Project Management](/handbook/product/categories/#project-management-group), please
feel free to reach out to Gabe Weaver via [e-mail](mailto:gweaver@gitlab.com)

## Upcoming

#### How we prioritize

We use the following decision framework when evaluating what to work on. Each idea must meet all three of the minimum criteria and two of the extreme criteria.

|                  | Criteria 1                          | Criteria 2                           | Criteria 3                    |
| ---------------- | ----------------------------------- | ------------------------------------ | ----------------------------- |
| Minimum (3 of 3) | simplifies collaboration | reduces the friction to contributing | aligned with market direction |
| Extreme (2 of 3) | enhances the ability to build trust | decreases manual effort | increases clarity and purpose |

### Next Few Releases

This is a list of epics that we are working on in **FY20-Q4** (November to January). More information on the currently prioritized items can be found within the Category level direction pages.

#### Issue Tracking

- [Manage sprint and release cadence](https://gitlab.com/groups/gitlab-org/-/epics/69)
- [Issue Task Lists and Tasks](https://gitlab.com/groups/gitlab-org/-/epics/543)
- [Team velocity and volatility](https://gitlab.com/groups/gitlab-org/-/epics/435)
- [Custom Fields](https://gitlab.com/groups/gitlab-org/-/epics/235)

#### Kanban Boards

- [Real-time Boards](https://gitlab.com/groups/gitlab-org/-/epics/382)
- [Integrated analytics on Issue Boards](https://gitlab.com/groups/gitlab-org/-/epics/1956)
- [Custom Workflows](https://gitlab.com/groups/gitlab-org/-/epics/364)

### One Year Plan

We've written a [mock press release](/direction/plan/one_year_plan) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community. 

### Three Year Vision

The working format for this vision is a set of bullet points that represent major areas we ought to be really far along in solving by this point in time. 

- **Predictive Analytics** - Most project management tools are limited to providing analytics around “What Happened?” (See Sense and Respond diagram below). Those that stand out from the crowd in the coming years will be adept at using historical data to answer questions regarding “Why did it happen?”, “What will happen?”, and “What is the best that could happen?”. Because we are a complete and unified DevOps solution, we are sitting on an incredibly rich data set that can be data scientists can harness using advanced statistics and machine learning to help companies more strategically manage their projects, products, initiatives, and overall value stream.
- **Death To Context Switching** - Whether a team is conducting a daily standup, retrospective, post mortem, or sprint planning, GitLab natively supports the facilitation of all of the major activities product teams undertake during a normal release cycle. GitLab keeps teams in maximum flow and intelligently adapts how information is presented dependent on what decision a team needs to make or what problems they are trying to solve. 
- **Agile Fluency** - Only 6% of all organizations that have adopted some form of agile practices report high levels of maturity and greater adaptability to changing market conditions. Our planning tools not only help teams practice popular agile methodologies, but predictably progress through each zone in the [Agile Fluency Model](https://martinfowler.com/articles/agileFluency.html) in a measurable way. 
- **Continuous Feedback & Product Lifecycle Management** - What happens once a feature ships? An issue is closed and eventually someone might go look at some analytics. At some point, a bug or more gets filed, users provide some level of feedback, and a product manager iterates haphazardly to improve upon it. The problem is this takes months, is undisciplined in most cases, and is incredibly difficult to manage. GitLab solves this by extending the DevOps model to Product Management. GitLab now provides a full fledged analytics service and robust feedback collection tools that yield incredibly complex data points visualized over an innovative, simple map of the surface area of your product(s); enabling revolutionary insights on customer flow, what’s working, what’s not working, and where to focus improvement efforts to substantially increase the ROI for a given feature set. 

![Sense and Respond Diagram](sense-and-respond.png)