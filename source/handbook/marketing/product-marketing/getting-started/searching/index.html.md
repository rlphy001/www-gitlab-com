---
layout: markdown_page
title: "Searching The GitLab Website like a pro"
---

## Challenge

At GitLab, we're prolific at documenting what we do in the handbook, the website, and in GitLab documentation.  This may make it difficult to find specific pieces of content.

## Basic Solution -  "search site:"

Google already indexes all our public facing pages and there is a [search modifier google offers that will help](https://support.google.com/websearch/answer/2466433?hl=en).

> **Search for a specific site**<br>
Put "site:" in front of a site or domain. For example, `site:youtube.com`.

If you are looking for information on the GitLab "team", then simply type this into the Google search:<br><br> `team site:about.gitlab.com`

The google search results will be **only** from `about.gitlab.com`.

Similarly, if you're looking for "permissions" in GitLab product documentation, then simply type this into search: <br><br>`permissions site:docs.gitlab.com`

The google search results will be **only** be documentation from `docs.gitlab.com`.

This also works for specific subdirectories. For example, if you **only** want to search in the handbook for "values" you can type 

`site:about.gitlab.com/handbook/values`

## Advanced Solution

The `site:` technique is incredibly powerful.  But, if you use it often, you end up typing the URL all the time - not very efficient.  If you use Chrome, there is a simple trick to use Chrome's search engine configuration to eliminate having to type `site:about.gitlab.com` every time.   You only have to type 3 characters.  Here's how.

**First:** Configure a new Search Engine shortcut in Chrome

| Step	 | Image  |
|---------------|----------------|
| 1. Right click on the URL field in Chrome and select "Edit Search Engines  |   ![Edit search engine](/images/gettingstarted/search/1_edit_search_engine.png)  |
| 2. In the Dialog, click `Add`  |   ![Add search engine](/images/gettingstarted/search/2_add_search_engine.png) |
| 3. There are THREE fields in the **Edit search engine** dialog. <br>     a. In the *Search Engine* field Enter `GitLab`<br>     b. In the *Keyword* field, enter `gl`<br>     c. In the *URL* field enter `http://www.google.com/search?q=%s%20site:about.gitlab.com`<br>  d. Click Save. | ![Enter search engine details](/images/gettingstarted/search/3_enter_search_engine_details.png) |

**Then** Go to a new Chrome tab and test it.

| Step	 | Image  |
|---------------|----------------|
| 1. In the Chrome URL/search field type `gl`  | ![Use Keyword to search](/images/gettingstarted/search/4_search_gl.png) |
| 2. AND then press `space`.  Notice how the field changes to indicate the selected search engine. | ![Press space key](/images/gettingstarted/search/5_search_space.png) |
| 3. Now, your **new** GitLab search engine will search using the `site:about.gitlab.com` modifier. | ![Search results from Gitlab](/images/gettingstarted/search/6_search_results.png) |


### What about GitLab documentation?

Simple. create another search engine.

| Search GitLab Documentation	 | Image  |
|---------------|----------------|
| 1. In the **Edit search engine** dialog. <br>   2. In the *Search Engine* field Enter `GitLab docs` <br>   3. In the *Keyword* field, enter `gd` <br>    4. In the *URL* field enter <br> `http://www.google.com/search?q=%s%20site:docs.gitlab.com` |    ![Settings for docs.gitlab.com](/images/gettingstarted/search/7_gitlab_docs_search.png) |

### What about Finding GitLab Issues?

Yep, create another search engine shortcut - this one will search for Issues in gitlab.com

| Search GitLab Issues	 | Image  |
|---------------|----------------|
| 1. In the **Edit search engine** dialog. <br>   2. In the *Search Engine* field Enter `GitLab.com issues` <br>   3. In the *Keyword* field, enter `gg` <br>    4. In the *URL* field enter  <br> `https://gitlab.com/search?search=%s&project_id=&group_id=6543`  (Note this restricts the search to the gitlab.com group) |    ![Settings for docs.gitlab.com](/images/gettingstarted/search/8_gitlab_issues_search.png) |


### What about Finding files in Google Docs??

You guessed it. Simply create another search engine shortcut - this one will search for documents in Google Docs

| Search Google Docs	 | Image  |
|---------------|----------------|
| 1. In the **Edit search engine** dialog. <br>   2. In the *Search Engine* field Enter 'Google Drive (dv)' <br>   3. In the *Keyword* field, enter `dv` <br>    4. In the *URL* field enter <br> `https://drive.google.com/drive/search?q=%s` |    ![Settings for docs.gitlab.com](/images/gettingstarted/search/9_google_drive_search.png) |

