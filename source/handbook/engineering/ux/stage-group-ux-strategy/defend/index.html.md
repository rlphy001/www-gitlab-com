---
layout: handbook-page-toc
title: "Defend UX"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Defend stage includes all features that help you defend your applications and cloud infrastructure by giving you the ability to identify, catalogue, manage, and remediate threats, vulnerabilities, and risks. For more information about our principles and upcoming features, see our [Product Vision](/direction/defend/) page.

While the [Secure UX](/handbook/engineering/ux/stage-group-ux-strategy/secure/) team’s goal is to provide the best experience in taking pre-emptive security measures before deploying your code, the Defend UX team’s goal is to provide the best experience in keeping your application safe after your code is in production.

## Our customer

Organizations of all sizes benefit from our tool and the experience of bringing teams together. We provide customers value with workflow efficiency, informed team decision-making, lower risk of security breaches, and attaining compliance requirements. We focus on all aspects of the product — starting with the customer experience. When deciding to use our tool, organizations are often considering the following:
- What languages does the tool support?
- What tests do we need to cover?
- What tests does the tool cover?
- Can it be automated?
- How long will setup take?
- What does setup involve?
- How easy is it to use?
- What technologies do you need to use? ex. Docker, Kubernetes
- How lightweight is the tool?
- How does it integrate with our tools?
- What customer support is offered?
- What are the upcoming features? (we are selling contracted services vs monthly)

## Our user

The Defend user is responsible for maintaining the security of their company’s environments and/or applications, through both proactive and reactive measures. They prefer to be proactive by abstracting away from manual, repetitive tasks and moving towards automation. 

The Defend user is responsible for risk mitigation, remediation, documenting their processes in timelines and runbooks, collaborating with other teams, meeting compliance standards, and performing root cause analyses. 

Typically, these user roles are: 

- SecOps Engineer
- [Security Analyst](/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst) 

## Our UX Scorecards 

Primary Jobs To Be Done (JTBD): 
- When I make sure my company’s applications aren’t vulnerable to bad actors, I want to monitor the traffic coming to my application and detect the possibility of an attack (SQL injection attempts, XSS attempts, vulnerability scanners, etc) so I can know what parts of the application I need to protect better. 
- When a bad actor successfully attacks my system, I want to look at the WAF logs to determine the attack vector (how they got in) so I can secure the weak points in my application.  
- When I want to make sure I’m being successful in my role, I want to to be able to confirm if a data breach happened, if any systems went down as a result of an attack, to what extent our security controls negatively impact uptime or throughput, and how much maintenance did the security controls require this quarter, so I can track and meet my OKRs.
- When I want to avoid putting out constant fires, I want to be proactive by creating tooling to understand events occurring in my application so I can catch security risks before they can be compromised.
- When our proactive security measures fail, I want to prioritize incident response so I can keep our environments (and our customer data) safe.
- When I’m optimizing my company’s processes, I want to make sure everyone is updating the runbooks so I can make sure the team is being efficient and following the same validated procedures.
- When I want to demonstrate that a procedure is in place, I want to invest time in compliance so I can help our Security Compliance team provide information and assurance about our information security program to customers and remove security as a barrier to adoption by them.
- When I’m conducting incident management, I need to communicate well with other teams and know when to involve them so I can put fixes in place quickly and efficiently.

## Our team
Our team continues to grow. We currently have 4 members that contribute to Defend UX efforts:
- [Valerie Karnes](https://gitlab.com/vkarnes) - UX Manager
- [Andy Volpe](https://gitlab.com/andyvolpe) - Sr. Product Designer
- [Becka Lippert](https://gitlab.com/beckalippert) - Product Designer
- [Tali Lavi](https://gitlab.com/tlavi) - UX Researcher

Our team meetings:
- The Defend team has weekly meetings alternating Tuesdays at 10:30am EST and Tuesdays at 4:00pm EST to discuss stage progress and updates across UX, PM, and Engineering.
- Secure and Defend Design Review meetings occur weekly at 11:00am EST to discuss our stages UX shared efforts, review designs, and iterate on our strategy.

## How we work
We use [labels](/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels) to help manage and track our work. 

## Our strategy
We are working together to [uncover customers core needs](https://gitlab.com/groups/gitlab-org/-/epics/1611), what our users’ workflows look like, and defining how we can make our users tasks easier. Our strategy involves the following actions:
- [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) (quarterly)
- Internal understanding: stakeholder interviews (annually)
- Iterating on Defend’s [product foundation's document](https://docs.google.com/document/d/1xGo870l7wqA4RFo-P_691PEq-Zqn_q0FcWi_620T4Ec/edit?usp=sharing) (ongoing)
- Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
- We talk to our customers (ongoing)
- We talk to our users (ongoing)
- We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
- Testing our features with usefulness and usability studies
- Dogfooding and partnering closely with our internal Security teams for feedback and feature adoption
- Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
- Collaborating with stakeholders on proof of concept discoveries to better understand future consideration
- Collaborating with the Secure team to make sure we’re offering a consistent and cohesive security workflow so that our customers can apply reactive findings into proactive measures and make sure their applications are protected throughout the entire application lifecycle 
- Prioritizing issues that are likely to increase our number of active users

The source of truth lives with shipped features, therefore we:
- Make data-driven decisions quickly and thoughtfully
- Optimize to deliver our solutions as soon as possible
- Learn, iterate, test, and repeat


## Follow our work
You can find us on the [Secure and Defend UX YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KrFCe5BgUkzFrZifjforQOz) to see UX Scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.



