---
layout: markdown_page
title: "Spinnaker"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes.  Created at Netflix, it has been battle-tested in production by hundreds of teams over millions of deployments. It combines a powerful and flexible pipeline management system with integrations to the major cloud providers. ([source:](https://www.spinnaker.io/))

Like Spinnaker, GitLab is also open source and also provides a flexible CD pipeline management system, is able to deploy to major cloud providers, and also offers advanced deployment patterns such as canary and incremental (rolling) deployments. In addition to this, GitLab provides not just the CD portion of the SDLC, but also everything else from planning, to CI, to testing, packaging, monitoring, and security scanning, all in a single application.


Spinnaker provides two core sets of features:

1. Application management: This is primarily to view and control your cloud resources.  In Spinnaker an Application is modeled along the lines of Microservices.  An application contains clusters, which consists of Server Groups.  The Application also models Load Balancers and Firewalls.

2. Application deployment:  This construct is used to model and manage continuous delivery workflows.  A key element of Application Deployment is the Pipeline.  A pipeline consists of a series of actions or stages which can be executed manually or automatically triggered.  Parameters can also be passed from one stage to another and used for further actions.

At a macro level Spinnaker provides a normalized taxonomy of artifacts and actions that can apply across cloud providers.  Using this normalized taxonomy one can build complex pipelines for a cloud provider or across cloud providers.

## Strengths

* Easy application deployment across cloud providers
* Pre-packaged actions that can be easily applied, without need for lower level understanding of different cloud provider's concepts
* Spinnaker is Open Source and has a community of developers contributing to the project.

## Weaknesses

* Only supports the Continuous Delivery (CD) phase of a DevOps cycle.
* Normalized taxonomy does not apply across cloud providers.  For example, an Amazon ECS cluster does not map to any core Spinnaker concept.  So these need to be set up separately, thus diminishing the value that Spinnaker can provide.
* Cloud providers differ in how they deploy and manage applications, thus eventually users need to get down to individually understanding each provider.
* Spinnaker does not support OpenStack.
* Only available in Self-hosted and managed option.  No Cloud version of Spinnaker.
* Steeper learning curve - Pipeline expression language uses a custom syntax, which entails a learning curve for users.  Moreover, some string manipulation and other expression evaluation requires use of java code within the expression language.  Nested expressions are not supported in Pipeline expression logic, this can be a limitation in some scenarios.

## Resources
* [Pipeline Expression Reference](https://www.spinnaker.io/reference/pipeline/expressions/)
* [Spinnaker Github Repo](https://github.com/spinnaker)

## Comparison
